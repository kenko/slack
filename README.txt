- to compile:

    $ cabal sandbox init   # not necessary but advisable
    $ cabal install --only-dependencies
    $ cabal build

- to run:

    $ dist/build/slack/slack

  then go to http://localhost:8080/static/index.html

Tested with GHC 7.8; servant hasn't been tested with earlier versions
of GHC so it may not work / cabal may not be able to install all
dependencies.

