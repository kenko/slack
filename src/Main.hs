{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DeriveGeneric #-}
import Network.Wreq (get, responseBody)
import Network.Wai.Handler.Warp (run)
import Network.URI (uriRegName, uriAuthority, parseURI)
import qualified Data.Text as T
import qualified Text.HTML.TagSoup as TS
import Control.Monad
import Control.Lens ((^.))
import Control.Exception (catch)
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Char8 as C
import Data.Monoid ((<>), mempty)
import Data.Aeson (ToJSON, FromJSON)
import Data.Proxy (Proxy(..))
import Network.HTTP.Client (HttpException(..))
import Data.List (intersperse)
import Servant
import Servant.Utils.StaticFiles (serveDirectory)
import Servant.JQuery (jsForAPI)
import qualified Data.Map.Strict as M
import Control.Monad.State (State(..), modify, runState)
import Control.Monad.Except (ExceptT(..), runExceptT, throwError)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Either (EitherT(..))
import GHC.Generics

type StaticAPI = "static" :> Raw
type TagsAPI = "pagetags" :> ReqBody '[JSON] ApiRequest :> Post '[JSON] ApiResponse

type API = StaticAPI :<|> TagsAPI

data Error = InvalidScheme | LocalAddress | NoAuthority | UnparseableURI | HttpError HttpException deriving (Show)

data ApiRequest = ApiRequest { uri :: T.Text } deriving Generic

instance FromJSON ApiRequest

data ApiResponse = ApiResponse { page :: Maybe String
                               , status :: String
                               , tags :: [(String, Int)]
                               , message :: Maybe String
                               } deriving Generic
instance ToJSON ApiResponse

toString :: ByteString -> String
toString = C.unpack . L.toStrict

validURI s = do
  uri <- maybe (throwError UnparseableURI) return $ parseURI s
  unless (validScheme . uriScheme $ uri) $ throwError InvalidScheme
  flip (maybe (throwError NoAuthority)) (uriAuthority uri) $ \authority -> do
    unless (isLocal . uriRegName $ authority) $ throwError LocalAddress
    return uri

  where
    validScheme s = s == "http:" || s == "https:"    
    isLocal s = s /= "127.0.0.1" && s /= "localhost"

tryGet uri = fmap Right (get uri) `catch` (return . Left . HttpError)

pageTags uri = do
  validURI uri
  response <- ExceptT $ liftIO $ tryGet uri
  let body = response ^. responseBody
  return $ TS.parseTags body

renderTags :: [TS.Tag ByteString] -> State (M.Map ByteString Int) ByteString
renderTags (TS.TagOpen tagname attrs:rest) = do
  let tagname' = L.map (\i -> if i >= 65 && i <= 90 then i + 32 else i) tagname
  let modaction = case L.uncons tagname' of
        Nothing -> return ()
        Just (33, _) -> return () -- don't treat doctype as a tag
        _ -> modify (M.insertWith (+) tagname' 1)
  modaction
  rendered <- renderTags rest
  return $ TS.renderTags ([ TS.TagText "<"
                          , TS.TagOpen "span" [("class", "tag_" <> tagname')]
                          , TS.TagText tagname
                          , TS.TagClose "span" 
                          ] <> intersperse' (TS.TagText " ") (renderAttrs attrs)
                            <> [TS.TagText ">"])
    <> rendered
  where
    intersperse' spacer [] = []
    intersperse' spacer xs = spacer : intersperse spacer xs
    renderAttrs [] = []
    renderAttrs ((t,""):rest) = [TS.TagText t] <> renderAttrs rest
    renderAttrs ((t,s):rest)  = [TS.TagText (t <> "=\"" <> s <> "\"")] <> renderAttrs rest
renderTags (TS.TagClose tagname:rest) = do
  rendered <- renderTags rest
  return $ TS.renderTags [ TS.TagText ("</" <> tagname <> ">") ] <> rendered
renderTags (t:rest) = do
  rendered <- renderTags rest
  return $ TS.renderTags [t] <> rendered
renderTags [] = return mempty

getTags :: ApiRequest -> EitherT ServantErr IO ApiResponse
getTags (ApiRequest uri) = do
  let uri' = T.unpack uri
  tags <- runExceptT $ pageTags uri'
  case tags of 
   Right tags' -> let (rendered, all_tags) = runState (renderTags tags') M.empty in
                  return ApiResponse { page = Just $ toString rendered
                                     , tags = map (\(k, v) -> (toString k, v)) $ M.toList all_tags
                                     , status = "ok"
                                     , message = Nothing
                                     }
   Left err -> return ApiResponse { status = "error"
                                  , message = Just $ show err
                                  , page = Nothing
                                  , tags = []
                                  }

server :: Server API
server = serveDirectory "static" :<|> getTags

api :: Proxy API
api = Proxy

app = serve api server

server' :: Server TagsAPI
server' = undefined
api' :: Proxy TagsAPI
api' = Proxy

apijs = jsForAPI api'

main = do
  writeFile "static/api.js" apijs
  run 8080 app
