
function postpagetags(body, onSuccess, onError)
{
  $.ajax(
    { url: '/pagetags'
    , success: onSuccess

    , data: JSON.stringify(body)
    , error: onError
    , type: 'POST'
    });
}
