// this is what servant-jquery WOULD generate, if it set the
// contentType correctly.
function postpagetags(body, onSuccess, onError)
{
  $.ajax(
    { url: '/pagetags'
      , success: onSuccess
      , contentType: "application/json"
      , data: JSON.stringify(body)
      , error: onError
      , type: 'POST'
    });
}

var last_class = null;
function highlight_class(cls) {
  if(last_class) {
    $("."+last_class).removeClass("highlighted");
  }
  last_class = cls;
  $("."+cls).addClass("highlighted");
}

function get_tags(uri) {
  $error = document.getElementById("error")
  $error.style.display = 'none';
  postpagetags({'uri': uri}, function (resp) {
    if (resp.status === "ok") {
      $pre = $("#pagetext");
      $pre.html(resp.page);
      i = 0;
      ol = document.createElement("ol");
      while (i < resp.tags.length) {
        p = document.createElement("p");
        a = document.createElement("a");
        count = document.createTextNode(" (" + resp.tags[i][1] + ")");
        a.href = "#";
        a.textContent = resp.tags[i][0];
        li = document.createElement("li");
        p.appendChild(a)
        p.appendChild(count)
        li.appendChild(p);
        ol.appendChild(li);
        i += 1;
      }
      $("#tagslist").empty().append(ol);
      $("a").click(function () { 
        highlight_class("tag_" + (this.textContent || this.innerText)) 
      } )
    } else {
      $("#tagslist").empty();
      $("#pagetext").empty();
      error.innerText = "ERROR: " + resp.message;
      error.style.display = '';
    }
  });
}
